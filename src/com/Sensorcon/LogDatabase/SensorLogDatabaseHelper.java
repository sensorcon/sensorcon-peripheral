package com.Sensorcon.LogDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


public class SensorLogDatabaseHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "sensorperiph.db";
    private static final int DATABASE_VERSION = 2;

    public static final String TABLE_LOGS = "logs";
    public static final String COLUMN_LOGS_ID = "_id";
    public static final String COLUMN_LOGS_TIMESTAMP = "timestamp";
    public static final String COLUMN_LOGS_MAC = "mac";
    public static final String COLUMN_LOGS_TEMP = "temp";
    public static final String COLUMN_LOGS_HUMIDITY = "humidity";
    public static final String COLUMN_LOGS_PRESSURE = "pressure";
    public static final String COLUMN_LOGS_CO = "co";
    public static final String COLUMN_LOGS_LIGHT = "light";

    public static final String TABLE_DEVICES = "devices";
    public static final String COLUMN_DEVICES_ID = "_id";
    public static final String COLUMN_DEVICES_MAC = "mac";
    public static final String COLUMN_DEVICES_NAME = "name";
    public static final String COLUMN_DEVICES_LOC = "loc";

    private static final String TABLE_LOGS_CREATE = "create table " + TABLE_LOGS + " ("
            + COLUMN_LOGS_ID + " integer primary key autoincrement, "
            + COLUMN_LOGS_TIMESTAMP + "  text not null,"
            + COLUMN_LOGS_MAC + " text not null,"
            + COLUMN_LOGS_TEMP + " real,"
            + COLUMN_LOGS_HUMIDITY + " real,"
            + COLUMN_LOGS_PRESSURE + " real,"
            + COLUMN_LOGS_CO + " real,"
            + COLUMN_LOGS_LIGHT + " real);";

    private static final String TABLE_DEVICES_CREATE = "create table " + TABLE_DEVICES + " ("
            + COLUMN_DEVICES_ID + " integer primary key autoincrement, "
            + COLUMN_DEVICES_MAC + "  text not null,"
            + COLUMN_DEVICES_NAME + "  text not null,"
            + COLUMN_DEVICES_LOC + "  text not null);";

    public SensorLogDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_LOGS_CREATE);
        db.execSQL(TABLE_DEVICES_CREATE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_LOGS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICES);
        onCreate(db);
    }
}

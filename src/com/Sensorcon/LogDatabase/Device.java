package com.Sensorcon.LogDatabase;


public class Device {

    private long mId;
    private String mMACAddress;
    private String mName;
    private String mLocation;

    public long getId() {
        return mId;
    }

    public void setId(long id) {
        mId = id;
    }

    public String getMACAddress() {
        return mMACAddress;
    }

    public void setMACAddress(String address) {
        mMACAddress = address;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }

    public String getLocation() {
        return mLocation;
    }

    public void setLocation(String location) {
        mLocation = location;
    }

    public String toString() {
        String st = "[MAC " + mMACAddress + "]";
        st += " [Name " + mName + "]";
        st += " [Location " + mLocation + "]";

        return st;
    }
}

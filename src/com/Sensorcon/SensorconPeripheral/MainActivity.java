package com.Sensorcon.SensorconPeripheral;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import android.widget.Button;


@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class MainActivity extends Activity {

    // Interface variables
    private Button btnScan;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        btnScan = (Button) findViewById(R.id.btnScan);

        btnScan.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                scan();
            }
        });
    }

    private void scan() {

        // If android version is less than 5.0, a different method of scanning is used
        if (Build.VERSION.SDK_INT < 21) {
            final Intent intent = new Intent(this, DeviceScanActivityOld.class);
            startActivity(intent);
        } else {
            final Intent intent = new Intent(this, DeviceScanActivity.class);
            startActivity(intent);
        }
    }
}

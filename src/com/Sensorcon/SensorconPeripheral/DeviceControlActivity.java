package com.Sensorcon.SensorconPeripheral;

import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import com.Sensorcon.LogDatabase.SensorLog;
import com.Sensorcon.LogDatabase.SensorLogDataSource;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class DeviceControlActivity extends Activity {

    private final static String TAG = DeviceControlActivity.class.getSimpleName();

    public static final String EXTRAS_DEVICE_NAME = "DEVICE_NAME";
    public static final String EXTRAS_DEVICE_ADDRESS = "DEVICE_ADDRESS";

    private TextView tvConnectionState;
    private TextView tvTemperature;
    private TextView tvHumidity;
    private TextView tvPressure;
    private TextView tvLight;
    private TextView tvCO;
    private String mDeviceName;
    private String mDeviceAddress;
    private BluetoothLeService mBluetoothLeService;
    private ArrayList<ArrayList<BluetoothGattCharacteristic>> mGattCharacteristics =
            new ArrayList<ArrayList<BluetoothGattCharacteristic>>();
    private boolean mConnected = false;

    SensorLogDataSource db;

    private ProgressDialog progressBar;
    private ProgressDialog progressBarQuery;
    private int progressBarStatus = 0;
    private Handler progressBarHandler = new Handler();

    private File csv = null;
    private File root = Environment.getExternalStorageDirectory();
    private FileOutputStream out;

    // Code to manage Service lifecycle.
    private final ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            mBluetoothLeService = ((BluetoothLeService.LocalBinder) service).getService();
            if (!mBluetoothLeService.initialize()) {
                Log.e(TAG, "Unable to initialize Bluetooth");
                finish();
            }
            // Automatically connects to the device upon successful start-up initialization.
            mBluetoothLeService.connect(mDeviceAddress);
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mBluetoothLeService = null;
        }
    };

    // Handles various events fired by the Service.
    // ACTION_GATT_CONNECTED: connected to a GATT server.
    // ACTION_GATT_DISCONNECTED: disconnected from a GATT server.
    // ACTION_GATT_SERVICES_DISCOVERED: discovered GATT services.
    // ACTION_DATA_AVAILABLE: received data from the device.  This can be a result of read
    //                        or notification operations.
    private final BroadcastReceiver mGattUpdateReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            final String action = intent.getAction();
            if (BluetoothLeService.ACTION_GATT_CONNECTED.equals(action)) {
                mConnected = true;
                updateConnectionState(R.string.connected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_DISCONNECTED.equals(action)) {
                mConnected = false;
                updateConnectionState(R.string.disconnected);
                invalidateOptionsMenu();
            } else if (BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED.equals(action)) {
                // Show all the supported services and characteristics on the user interface.
                saveGattServices(mBluetoothLeService.getSupportedGattServices());

                // The 8th characteristic in the sensor service holds all of the sensor values
                // This characteristic will be used for notifications to stream the sensors

                int size = mGattCharacteristics.get(3).size();
                BluetoothGattCharacteristic characteristic = mGattCharacteristics.get(3).get(size - 1);

                mBluetoothLeService.setCharacteristicNotification(
                        characteristic, true);
            } else if (BluetoothLeService.ACTION_DATA_AVAILABLE.equals(action)) {

                // Get all sensors characteristic
                byte data[] = intent.getByteArrayExtra(BluetoothLeService.EXTRA_DATA);

                int index = 0;

                // Parse the data
                float temperature = (float) ((data[index++] & 0x000000FF) + ((data[index++] << 8) & 0x0000FF00));
                float humidity = (float)((data[index++] & 0x000000FF) + ((data[index++] << 8) & 0x0000FF00));
                float pressure = (float) ((data[index++] & 0x000000FF) + ((data[index++] << 8) & 0x0000FF00) + ((data[index++] << 16) & 0x00FF0000));
                float co = (float) ((data[index++] & 0x000000FF) + ((data[index++] << 8) & 0x0000FF00));
                float light = (float) ((data[index++] & 0x000000FF) + ((data[index++] << 8) & 0x0000FF00) + ((data[index++] << 16) & 0x00FF0000) + ((data[index++] << 24) & 0xFF000000));

                String tempString;
                String humidityString;
                String pressureString;
                String lightString;
                String coString;

                // Check for overload conditions
                if(temperature == 55538) {
                    tempString = "OL";
                } else if(temperature == 55537) {
                    tempString = "-OL";
                } else {
                    tempString = String.format("%.1f", temperature/10);
                }

                if(humidity == 55538) {
                    humidityString = "OL";
                } else if(humidity == 55537) {
                    humidityString = "-OL";
                } else {
                    humidityString = String.format("%.1f", humidity/10);
                }

                if(pressure == 16777215) {
                    pressureString = "OL";
                } else if(pressure == 16777214) {
                    pressureString = "-OL";
                } else {
                    pressureString = String.format("%.1f", pressure/10);
                }

                if(light == -1) {
                    lightString = "OL";
                } else {
                    lightString = String.format("%.1f", light/10);
                }

                // Check for overload conditions
                if(co == 55538) {
                    coString = "OL";
                } else {
                    coString = String.format("%.1f", co/10);
                }

                // Extended timestamp with date for database
                SimpleDateFormat sdf2 = new SimpleDateFormat("MM-dd-yyyy hh:mm:ss a");
                String ts = sdf2.format(new Date());

                db.createLog(ts, mDeviceAddress, temperature, humidity, pressure, co, light);

                // Display sensor data on interface
                displayData(tempString, humidityString, pressureString, lightString, coString);
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.connected);

        db = new SensorLogDataSource(this);
        try {
            db.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Write dummy log to place where this session started so that current sessions can
        // be exported separately from the entire database. Make sure to remove the last
        // dummy log first
        db.deleteLog("01-01-1970 00:00:00 AM", "00:00:00:00:00:00");
        db.createLog("01-01-1970 00:00:00 AM", "00:00:00:00:00:00", 0, 0, 0, 0, 0);

        final Intent intent = getIntent();
        mDeviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        mDeviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);

        // Sets up UI references.
        tvConnectionState = (TextView) findViewById(R.id.tvStatus);
        tvTemperature = (TextView) findViewById(R.id.tvTemperature);
        tvHumidity = (TextView) findViewById(R.id.tvHumidity);
        tvPressure = (TextView) findViewById(R.id.tvPressure);
        tvLight = (TextView) findViewById(R.id.tvLight);
        tvCO = (TextView) findViewById(R.id.tvCO);

        getActionBar().setTitle(mDeviceName);
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Intent gattServiceIntent = new Intent(this, BluetoothLeService.class);
        bindService(gattServiceIntent, mServiceConnection, BIND_AUTO_CREATE);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mGattUpdateReceiver, makeGattUpdateIntentFilter());
        if (mBluetoothLeService != null) {
            final boolean result = mBluetoothLeService.connect(mDeviceAddress);
            Log.d(TAG, "Connect request result=" + result);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mGattUpdateReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbindService(mServiceConnection);
        mBluetoothLeService = null;
        db.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.database, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_export_session:
                exportCSV(true);
                return true;
            case R.id.menu_export_all:
                exportCSV(false);
                return true;
            case R.id.menu_delete:
                deleteAllLogs();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void updateConnectionState(final int resourceId) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvConnectionState.setText(resourceId);
            }
        });
    }

    /**
     * Updates the UI with new sensor values
     *
     * @param temperature
     * @param humiditiy
     * @param pressure
     * @param light
     * @param CO
     */
    private void displayData(final String temperature, final String humiditiy, final String pressure, final String light, final String CO) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                tvTemperature.setText(temperature);
                tvHumidity.setText(humiditiy);
                tvPressure.setText(pressure);
                tvLight.setText(light);
                tvCO.setText(CO);
            }
        });
    }

    /**
     * Saves all of the characteristics to a two dimensional list
     *
     * @param gattServices
     */
    private void saveGattServices(List<BluetoothGattService> gattServices) {
        if (gattServices == null) return;

        mGattCharacteristics = new ArrayList<ArrayList<BluetoothGattCharacteristic>>();

        // Loops through available GATT Services.
        for (BluetoothGattService gattService : gattServices) {

            List<BluetoothGattCharacteristic> gattCharacteristics =
                    gattService.getCharacteristics();
            ArrayList<BluetoothGattCharacteristic> charas =
                    new ArrayList<BluetoothGattCharacteristic>();

            // Loops through available Characteristics.
            for (BluetoothGattCharacteristic gattCharacteristic : gattCharacteristics) {
                charas.add(gattCharacteristic);
            }

            mGattCharacteristics.add(charas);
        }
    }

    private static IntentFilter makeGattUpdateIntentFilter() {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_CONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_DISCONNECTED);
        intentFilter.addAction(BluetoothLeService.ACTION_GATT_SERVICES_DISCOVERED);
        intentFilter.addAction(BluetoothLeService.ACTION_DATA_AVAILABLE);
        return intentFilter;
    }

    private void exportCSV(boolean session) {

        ArrayList<String> lineList = new ArrayList<String>();

        progressBarQuery = new ProgressDialog(this);

        progressBarQuery.setCancelable(false);
        progressBarQuery.setMessage("Querying Database...");
        progressBarQuery.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressBarQuery.setIndeterminate(true);

        progressBarQuery.show();

        ArrayList<SensorLog> logs = (ArrayList<SensorLog>) db.getAllLogs();

        progressBarQuery.dismiss();

        String tempString;
        String humidityString;
        String pressureString;
        String lightString;
        String coString;

        if(session == true) {
            ArrayList<SensorLog> sessionLogs = new ArrayList<SensorLog>();

            int index = 0;
            int count = 0;
            for(int i = 0; i < logs.size(); i++) {
                if(logs.get(i).getTimestamp().equals("01-01-1970 00:00:00 AM")) {
                    index = i;
                }

                if(index > 0) {
                    count++;
                }
            }

            for(int i = 0; i < count; i++) {
                sessionLogs.add(logs.get(index++));
            }

            String line = "";

            for(int i = 0; i < sessionLogs.size(); i++) {

                if (!sessionLogs.get(i).getTimestamp().equals("01-01-1970 00:00:00 AM")) {

                    // Check for overload conditions
                    if(sessionLogs.get(i).getTemperature() == 55538) {
                        tempString = "OL";
                    } else if(sessionLogs.get(i).getTemperature() == 55537) {
                        tempString = "-OL";
                    } else {
                        tempString = Float.toString(sessionLogs.get(i).getTemperature()/10);
                    }

                    if(sessionLogs.get(i).getHumidity() == 55538) {
                        humidityString = "OL";
                    } else if(sessionLogs.get(i).getHumidity() == 55537) {
                        humidityString = "-OL";
                    } else {
                        humidityString = Float.toString(sessionLogs.get(i).getHumidity()/10);
                    }

                    if(sessionLogs.get(i).getPressure() == 16777215) {
                        pressureString = "OL";
                    } else if(sessionLogs.get(i).getPressure() == 16777214) {
                        pressureString = "-OL";
                    } else {
                        pressureString = Float.toString(sessionLogs.get(i).getPressure()/10);
                    }

                    if(sessionLogs.get(i).getLight() == -1) {
                        lightString = "OL";
                    } else {
                        lightString = Float.toString(sessionLogs.get(i).getLight()/10);
                    }

                    // Check for overload conditions
                    if(sessionLogs.get(i).getCO() == 55538) {
                        coString = "OL";
                    } else {
                        coString = Float.toString(sessionLogs.get(i).getCO()/10);
                    }

                    line = sessionLogs.get(i).getTimestamp() + ","
                            + sessionLogs.get(i).getMACAddress() + ","
                            + tempString + ","
                            + humidityString + ","
                            + pressureString + ","
                            + coString + ","
                            + lightString;

                    lineList.add(line);
                }
            }

            generateCSV(lineList);
        } else {
            String line;

            for(int i = 0; i < logs.size(); i++) {

                // Check for overload conditions
                if(logs.get(i).getTemperature() == 55538) {
                    tempString = "OL";
                } else if(logs.get(i).getTemperature() == 55537) {
                    tempString = "-OL";
                } else {
                    tempString = Float.toString(logs.get(i).getTemperature()/10);
                }

                if(logs.get(i).getHumidity() == 55538) {
                    humidityString = "OL";
                } else if(logs.get(i).getHumidity() == 55537) {
                    humidityString = "-OL";
                } else {
                    humidityString = Float.toString(logs.get(i).getHumidity()/10);
                }

                if(logs.get(i).getPressure() == 16777215) {
                    pressureString = "OL";
                } else if(logs.get(i).getPressure() == 16777214) {
                    pressureString = "-OL";
                } else {
                    pressureString = Float.toString(logs.get(i).getPressure()/10);
                }

                if(logs.get(i).getLight() == -1) {
                    lightString = "OL";
                } else {
                    lightString = Float.toString(logs.get(i).getLight()/10);
                }

                // Check for overload conditions
                if(logs.get(i).getCO() == 55538) {
                    coString = "OL";
                } else {
                    coString = Float.toString(logs.get(i).getCO()/10);
                }

                if (!logs.get(i).getTimestamp().equals("01-01-1970 00:00:00 AM")) {
                    line = logs.get(i).getTimestamp() + ","
                            + logs.get(i).getMACAddress() + ","
                            + tempString + ","
                            + humidityString + ","
                            + pressureString + ","
                            + coString + ","
                            + lightString;

                    lineList.add(line);
                }
            }

            generateCSV(lineList);
        }
    }

    private void deleteAllLogs() {
        db.deleteAll();
        db.createLog("01-01-1970 00:00:00 AM", "00:00:00:00:00:00", 0, 0, 0, 0, 0);
    }

    /**
     * Creates csv file
     */
    private void generateCSV(final ArrayList<String> lines) {

        progressBar = new ProgressDialog(this);

        progressBar.setCancelable(false);
        progressBar.setMessage("Generating CSV file...");
        progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressBar.setIndeterminate(false);
        progressBar.setProgress(0);

        progressBar.show();

        new Thread(new Runnable() {
            public void run() {

                progressBar.setMax(lines.size());

                // Create CSV
                if(root.canWrite()) {

                    // Get a timestamp to display
                    SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy_hh:mm:ss");
                    Date now = new Date();

                    // Set up the display strings
                    String ts = sdf.format(now);
                    String fileName = "SensorconDataLog_" + ts + ".csv";

                    File dir = new File (root.getAbsolutePath() + "/SensorconDataLog");
                    dir.mkdirs();

                    csv = new File(dir, fileName);

                    try {
                        out = new FileOutputStream(csv);

                        String line;

                        line = "Time,MAC Address,Temperature,Humidity,Pressure,CO,Light\n";

                        // Header
                        out.write(line.getBytes());

                        for(int i = 0; i < lines.size(); i++) {

                            out.write(lines.get(i).getBytes());
                            out.write("\n".getBytes());

                            progressBarStatus++;

                            if ((progressBarStatus % 50) == 0) {
                                progressBarHandler.post(new Runnable() {
                                    public void run() {
                                        progressBar.setProgress(progressBarStatus);
                                    }
                                });
                            }
                        }

                        out.close();
                        progressBar.dismiss();

                        send();

                    } catch (IOException e) {
                        Log.d("error", e.getMessage());
                    }
                }
            }
        }).start();
    }

    /**
     * Sends csv to computer
     */
    private void send() {
        Uri fileUri = Uri.fromFile(csv);

        Intent sendIntent = new Intent(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Sensorcon Log Data");
        sendIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
        sendIntent.setType("text/html");
        startActivity(sendIntent);
    }
}
